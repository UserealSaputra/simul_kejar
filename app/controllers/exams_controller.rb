class ExamsController < ApplicationController
  def index
    @exams = Exam.all
  end

  def show
    id = params[:id]
    @exam = Exam.find(id)
  end

  def new
    @exam = Exam.new
  end

  def create
    exam = Exam.new(parameter)
    exam.save
    redirect_to exams_path
  end

  def edit
    @exam = Exam.find(params[:id])
  end

  def update
    @exam = Exam.find(params[:id])
    @exam.update(parameter)
    # puts book.errors.messages
    # flash[:notice] = 'A Book has been updated'
    redirect_to exam_path(@exam)
  end

  def destroy
    @exam = Exam.find(params[:id])
    @exam.destroy()
    # flash[:notice] = 'A Book has been deleted'
    redirect_to exams_path
  end

  private
  def parameter
    params.require(:exam).permit(:title, :duration, :nilai, :aktif, :level, :student_id, :mapel,) #ambil parameter dari inputan require
  end
end
