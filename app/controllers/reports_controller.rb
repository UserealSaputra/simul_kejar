class ReportsController < ApplicationController
  def index
    @repts = Report.all
  end

  def show
    id = params[:id]
    @rept = Report.find(id)
  end

  def new
    @rept = Report.new
  end

  def create
    rept = Report.new(parameter)
    rept.save
    redirect_to reports_path
  end

  def edit
    @rept = Report.find(params[:id])
  end

  def update
    @rept = Report.find(params[:id])
    @rept.update(parameter)
    # puts book.errors.messages
    # flash[:notice] = 'A Book has been updated'
    redirect_to report_path(@rept)
  end

  def destroy
    @rept = Report.find(params[:id])
    @rept.destroy()
    # flash[:notice] = 'A Book has been deleted'
    redirect_to reports_path
  end

  private
  def parameter
    params.require(:report).permit(:title, :hasil, :teacher_id, :student_id, :mapel, :date) #ambil parameter dari inputan require
  end
end
