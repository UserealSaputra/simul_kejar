class TeachersController < ApplicationController
  def index
    @teachs = Teacher.all
  end

  def show
    id = params[:id]
    @teac = Teacher.find(id)
  end

  def new
    @teac = Teacher.new
  end

  def create
    teac = Teacher.new(parameter)
    teac.save
    redirect_to teachers_path
  end

  def edit
    @teac = Teacher.find(params[:id])
  end

  def update
    @teac = Teacher.find(params[:id])
    @teac.update(parameter)
    # puts book.errors.messages
    # flash[:notice] = 'A Book has been updated'
    redirect_to teacher_path(@teac)
  end

  def destroy
    @teac = Teacher.find(params[:id])
    @teac.destroy()
    # flash[:notice] = 'A Book has been deleted'
    redirect_to teachers_path
  end

  private
  def parameter
    params.require(:teacher).permit(:nik, :name, :age, :kelas, :mapel) #ambil parameter dari inputan require
  end
end
