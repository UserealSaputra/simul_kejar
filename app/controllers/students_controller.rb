class StudentsController < ApplicationController
  def index
    @students = Student.all
  end

  def show
    id = params[:id]
    @stud = Student.find(id)
  end

  def new
    @stud = Student.new
  end

  def create
    stud = Student.new(parameter)
    stud.save
    redirect_to students_path
  end

  def edit
    @stud = Student.find(params[:id])
  end

  def update
    @stud = Student.find(params[:id])
    @stud.update(parameter)
    # puts book.errors.messages
    # flash[:notice] = 'A Book has been updated'
    redirect_to student_path(@stud)
  end

  def destroy
    @stud = Student.find(params[:id])
    @stud.destroy()
    # flash[:notice] = 'A Book has been deleted'
    redirect_to students_path
  end

  private
  def parameter
    params.require(:student).permit(:nik, :name, :username, :age, :kelas, :adress, :city) #ambil parameter dari inputan require
  end
end
