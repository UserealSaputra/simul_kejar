class CreatePayments < ActiveRecord::Migration[6.0]
  def up
    create_table :payments do |t|
      t.string :id_transaction
      t.string :status

      t.timestamps
    end
  end
end
