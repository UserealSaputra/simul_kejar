class CreatePockets < ActiveRecord::Migration[6.0]
  def up
    create_table :pockets do |t|
      t.integer :balance
      t.string :student_id
      t.string :teacher_id

      t.timestamps
    end
  end
end
