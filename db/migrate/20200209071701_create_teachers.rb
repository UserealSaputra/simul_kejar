class CreateTeachers < ActiveRecord::Migration[6.0]
  def up
    create_table :teachers do |t|
      t.string :nik
      t.string :name, limit: 100
      t.integer :age, default: 20
      t.string :kelas, limit: 80
      t.string :mapel

      t.timestamps
    end
  end
end
