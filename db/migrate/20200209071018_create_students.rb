class CreateStudents < ActiveRecord::Migration[6.0]
  def up
    create_table :students do |t|
      t.string :nik
      t.string :name
      t.string :username
      t.integer :age, default: 10
      t.string :kelas
      t.text :adress, default: 'Belum ada alamat'
      t.string :city

      t.timestamps
    end
  end
end
