class CreateExams < ActiveRecord::Migration[6.0]
  def up
    create_table :exams do |t|
      t.string :title
      t.string :mapel, limit: 100
      t.integer :duration, default:  0
      t.boolean :aktif, default: false
      t.float :nilai
      t.integer :level, default: 1
      t.string :student_id

      t.timestamps
    end
  end
end
