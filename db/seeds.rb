# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# Student data
Student.create({nik: 'A-001', name: 'Ardyn', age: 17, kelas: 'CL-01', adress: 'Jl. Retro', city: 'Bogor'})
Student.create({nik: 'A-002', name: 'Anduin', age: 16, kelas: 'CL-01', adress: 'Jl. Nano', city: 'Jakarta'})
Student.create({nik: 'A-003', name: 'Arnold', age: 15, kelas: 'CL-02', adress: 'Jl. Polo', city: 'Bogor'})
Student.create({nik: 'A-004', name: 'Alexis', age: 14, kelas: 'CL-02', adress: 'Jl. Uno', city: 'Jakarta'})
Student.create({nik: 'A-005', name: 'Anna', age: 18, kelas: 'CL-03', adress: 'Jl. Burito', city: 'Bogor'})
Student.create({nik: 'A-006', name: 'Alessa', age: 13, kelas: 'CL-03', adress: 'Jl. Marco', city: 'Jakarta'})

# Exam data
Exam.create(title: 'Ujian Kompetisi 1', mapel: 'Bahasa Indonesia', duration: 60, nilai: 70, aktif: true, level: 1, student_id: 'A-001')
Exam.create(title: 'Ujian Kompetisi 2', mapel: 'Bahasa Indonesia', duration: 90, nilai: 85, aktif: true, level: 2, student_id: 'A-002')
Exam.create(title: 'Ujian Kompetisi 3', mapel: 'Bahasa Indonesia', duration: 120, nilai: 93, aktif: true, level: 3, student_id: 'A-003')

# Teacher data
Teacher.create(nik: 'TC-001', name: 'Belarosa Hanna', age: 27, kelas: 'CL-01', mapel: 'Bahasa Indonesia')
Teacher.create(nik: 'TC-002', name: 'Robert Bob', age: 34, kelas: 'CL-02', mapel: 'Bahasa Inggris')
Teacher.create(nik: 'TC-003', name: 'Michael Johnson', age: 24, kelas: 'CL-03', mapel: 'Bahasa Jepang')

# Report
Report.create(title: 'Hasil Ujian Kompetisi 1', student_id: 'A-001', teacher_id: 'TC-001', mapel: 'Bahasa Indonesia', hasil: 70, date: '12-01-2020 12:30:00')
Report.create(title: 'Hasil Ujian Kompetisi 2', student_id: 'A-002', teacher_id: 'TC-001', mapel: 'Bahasa Indonesia', hasil: 85, date: '12-01-2020 12:30:00')
Report.create(title: 'Hasil Ujian Kompetisi 3', student_id: 'A-003', teacher_id: 'TC-001', mapel: 'Bahasa Indonesia', hasil: 93, date: '12-01-2020 12:30:00')
