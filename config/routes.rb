Rails.application.routes.draw do
  root 'welcome#index'
  resources :students
  resources :exams
  resources :teachers
  resources :reports

  # get 'reports/index'
  # get 'reports/show'
  # get 'reports/new'
  # get 'reports/edit'
  # get 'reports/update'
  # get 'reports/destroy'
  # get 'teachers/index'
  # get 'teachers/show'
  # get 'teachers/new'
  # get 'teachers/edit'
  # get 'teachers/update'
  # get 'teachers/destroy'
  # get 'exams/index'
  # get 'exams/show'
  # get 'exams/new'
  # get 'exams/edit'
  # get 'exams/update'
  # get 'exams/destroy'
  # get 'students/index'
  # get 'students/show'
  # get 'students/new'
  # get 'students/edit'
  # get 'students/update'
  # get 'students/destroy'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
